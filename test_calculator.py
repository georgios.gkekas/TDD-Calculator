import pytest
import calculator as calc

@pytest.mark.parametrize("key, expected_operation", [
    ('1', calc.add_numbers),
    ('2', calc.multiply_numbers),
    ('3', calc.divide_numbers),
    ('4', calc.subtract_numbers),
    ('5', exit)])
def test_select_operation(key, expected_operation):
    assert calc.select_operation(key) == expected_operation

def test_invalid_operation_raise_key_error():
    with pytest.raises(Exception):
        calc.select_operation(6)

def test_subtraction():
    assert calc.subtract_numbers(1, 1) == 0


def test_division_with_zero_raise_zero_division_error():
    with pytest.raises(Exception):
        calc.divide_numbers(1, 0)


def test_division():
    assert calc.divide_numbers(3, 1) == 3


def test_multiplication():
    assert calc.multiply_numbers(1, 1) == 1


def test_addition():
    assert calc.add_numbers(0, 1) == 1


'''Values like Nan and 0E0 if used in float function, they return numbers instead of error in conversion'''


@pytest.mark.parametrize("wrong_value", ["Nan", "str", "0E0", True, 12])
def test_input_string_raises_exception_on_calculation(wrong_value):
    with pytest.raises(Exception):
        calc.convert_input_to_float(wrong_value)
