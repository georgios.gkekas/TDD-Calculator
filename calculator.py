import re

def print_result(number1, operator, number2, result):
    print('Calculation: %s %s %s = %s' % (number1, operator, number2, result))

def convert_string_input_to_float(number):
    '''
    This function converts the string number to float.
    It starts by checking if the input contains chars, except for numbers and dots.
    This is to prevent from input like 'Nan' or 'True' which result into number and
    then it converts to float if it passes that check.

    :param number: the string number to be converted to float
    :return: the number converted to float
    '''
    try:
        if re.search("[^\d\.\-]", number) is not None:
            raise ValueError('The input number is not a number, please input a number.')
        return float(number)
    except Exception as error:
        raise error

def subtract_numbers(number1, number2):
    result = number1 - number2
    print_result(number1, '-', number2, result)
    return result

def add_numbers(number1, number2):
    result = number1 + number2
    print_result(number1, '+', number2, result)
    return result

def divide_numbers(number1, number2):
    if number2 == 0:
        raise ZeroDivisionError('Division with 0 is not allowed')
    result = number1 / number2
    print_result(number1, '/', number2, result)
    return result

def multiply_numbers(number1, number2):
    result = number1 * number2
    print_result(number1, '*', number2, result)
    return result

def select_operation(choice):
    switcher = {'1': add_numbers,
                '2': multiply_numbers,
                '3': divide_numbers,
                '4': subtract_numbers,
                '5': exit}
    if switcher.get(choice) is None:
        raise KeyError('Invalid operation, please use the number 1/2/3/4/5')
    return switcher.get(choice)

def calculator_run():
    while True:
        try:
            choice = input("Input number for calculation or exit:\n"
                           "1. Addition\n"
                           "2. Multiplication\n"
                           "3. Division\n"
                           "4. Subtraction\n"
                           "5. Exit")
            operation = select_operation(choice)
            if operation != exit:
                number1 = convert_string_input_to_float(input('Input the first number for the calculation: '))
                number2 = convert_string_input_to_float(input('Input the second number for the calculation: '))
                operation(number1, number2)
            else:
                exit(0)
        except Exception as error:
            print(error)

if __name__ == "__main__":
    calculator_run()





